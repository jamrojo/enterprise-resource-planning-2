<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
$autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');

$autoload['libraries'] = array('database','session', 'email', 'form_validation', 'upload', 'encryption', 'cipher', 'uuid', 'CSVReader', 'Ciqrcode', 'Messages', 'Stringtohex_hextostring');
$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'string', 'pdf_helper', 'file', 'directory');

$autoload['config'] = array();

$autoload['language'] = array(); 

$autoload['model'] = array('Sesion_model', 'Cuentas_model', 'Usuarios_model', 'Distribuidores_model', 'Admincuentas_model', 'Miperfil_model', 'Claves_model', 'Subcuentas_model', 'Invitaciones_model', 'Rcusuarios_model');
